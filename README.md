# Curso de Gitlab

Aqui veremos como utilizar Gitlab para gestion de proyectos, mejorar la gestion del tiempo del equipo, mejorar la calidad del codigo, integrar herramientas extra y en un futuro, ver lo basico del auto-devops de Gitlab

## Clase 1. - Gestion y reglas

En esta primera parte veremos mas que nada la parte de gestion de equipos de Gitlab, como se puede ver a grandes rasgos en que esta el proyecto, asignacion de issues, versionamiento, etc.

### Grupos y proyectos

Gitlab permite manejar organizaciones, separando los permisos en

1. Grupo:
Sería algo así como la organización (Ejemplo: Mediaagility)
2. Subgrupo:
Serían algo así como las divisiones/departamentos (Marketing, desarrollo, Data Science)
3. Proyectos
Se asignan a un subgrupo o grupo, dentro de los que los usuarios tendran permisos
diferentes para ver las cosas

> Nota: No abordé este tema a fondo por cuestiones de tiempo. Prometo completarlo

### Labels

Las etiquetas nos sirven para categorizar informacion de los issues, asignees en el y es tiempo que toma hacerlos

### Milestones

### Issues

Es la forma de saber en que esta el equipo, lo que le falta o sobra a la plataforma y
de distribuir el trabajo en equipos agiles.

Se pueden añadir a cada Issue también, con `/estimate` el tiempo estimado que debería tardar,
con `/spend` el tiempo gastado y tambien es posible agregar pesos.

Un peso (o `weight`) es un valor numerico que representa el *esfuerzo* del equipo, de forma
que podamos ver que tan importante es en el desarrollo

### Merge requests

#### Proceso

#### Normativas

#### Estándares de Commiteo

Se recomienda al hacer commit (habitualmente) tener un mensaje corto de leer y
que describa lo que estuviste haciendo.

Sirve para no perderse en el tiempo y poder identificar donde hubo cambios importantes,
dónde se pudieron haber roto cosas o simplemente para estadistica

Tambien se recomienda que cada commit tenga *cambios cortos*, para que el codigo
sea legible y se pueda decidir si el cambio se acepta o no.

A mí además de eso, personalmente me gusta seguir algunos estándares de
nombrado para saber que tipo de cambio es, sacados del
[Contribution guideline](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#type) de Angular

La idea es añadir un prefijo con el tipo de cambio que se esta haciendo.
Los prefijos recomendados son los mostrados a continuación.

- **BUILD**:
Cambios que afectan la construccion del sistema y/o dependencias externas (Ejemplo: gulp, broccoli, npm)
- **CI**:
Cambios a los archivos de configuracion y scripts de CI (Ejemplo: Circle, BrowserStack, SauceLabs)
- **DOCS**:
Cambios de solo documentacion
- **FEATURE**:
Un nuevo feature
- **FIX**:
Un bug resuelto
- **PERFORMANCE**:
Un cambio en el código que mejora el performance
- **REFACTOR**:
Un cambio en el codigo que ni elimina un bug ni añade un Feature
- **TEST**:
Para añadir tests faltantes o modifiar los existentes

### Integracion y cerrar issues

### Boards

### Service Desk


